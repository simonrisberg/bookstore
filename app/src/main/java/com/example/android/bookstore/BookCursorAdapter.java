package com.example.android.bookstore;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.android.bookstore.data.BookContract;
import com.example.android.bookstore.data.BookContract.BookEntry;

public class BookCursorAdapter extends CursorAdapter {



    public BookCursorAdapter(Context context, Cursor c){
        super(context, c, 0);
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        TextView nameTextView = (TextView) view.findViewById(R.id.name);
        TextView quantTextView = (TextView) view.findViewById(R.id.quantity);
        Button salesButton = (Button) view.findViewById(R.id.salesbutton);

        int nameColumnIndex = cursor.getColumnIndex(BookEntry.COLUMN_BOOK_NAME);
        int quantColumnIndex = cursor.getColumnIndex(String.valueOf(BookEntry.COLUMN_BOOK_QUANTITY));
        final int rowId = cursor.getInt(cursor.getColumnIndex(BookEntry._ID));

        String bookName = cursor.getString(nameColumnIndex);
        String bookQuantity = cursor.getString(quantColumnIndex);

        final int tempQuant = Integer.parseInt(bookQuantity);
        salesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quantity = tempQuant;
                if (quantity > 0){
                    quantity = quantity - 1;
                }
                ContentValues values = new ContentValues();
                Uri updateUri = ContentUris.withAppendedId(BookEntry.CONTENT_URI, rowId);
                values.put(BookEntry.COLUMN_BOOK_QUANTITY, quantity);
                context.getContentResolver().update(updateUri, values, null, null);
            }
        });

        bookQuantity = cursor.getString(quantColumnIndex);
        nameTextView.setText(bookName);
        quantTextView.setText(bookQuantity);



    }
}
