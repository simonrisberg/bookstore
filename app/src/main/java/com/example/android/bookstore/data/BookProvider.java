package com.example.android.bookstore.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.android.bookstore.data.BookContract.BookEntry;



public class BookProvider extends ContentProvider {

    public static final String LOG_TAG = BookProvider.class.getSimpleName();

    private static final int BOOKS = 100;

    private static final int BOOK_ID = 101;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {

        sUriMatcher.addURI(BookContract.CONTENT_AUTHORITY, BookContract.PATH_BOOKS, BOOKS);

        sUriMatcher.addURI(BookContract.CONTENT_AUTHORITY, BookContract.PATH_BOOKS + "/#", BOOK_ID);
    }

    private BookDbHelper mDbHelper;

    @Override
    public boolean onCreate() {
        mDbHelper = new BookDbHelper(getContext());
        return true;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase database = mDbHelper.getReadableDatabase();

        Cursor cursor;

        int match = sUriMatcher.match(uri);

        switch (match){
            case BOOKS:
                cursor = database.query(BookEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case BOOK_ID:
                selection = BookEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                cursor = database.query(BookEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Cannot query unknown URI " + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }


    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case BOOKS:
                return BookEntry.CONTENT_LIST_TYPE;
            case BOOK_ID:
                return BookEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri + " with match " + match);
        }
    }


    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case BOOKS:
                return insertPet(uri, contentValues);
            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);
        }
    }


    private Uri insertPet(Uri uri, ContentValues values){

        String name = values.getAsString(BookEntry.COLUMN_BOOK_NAME);
        if (name == null){
            throw new IllegalArgumentException("Book requires name");
        }

        String price = values.getAsString(BookEntry.COLUMN_BOOK_PRICE);
        if (name == null){
            throw new IllegalArgumentException("Book requires price");
        }

        Integer quantity = values.getAsInteger(String.valueOf(BookEntry.COLUMN_BOOK_QUANTITY));

        if (quantity != null && quantity < 0){
            throw new IllegalArgumentException("Book requires a quantity");
        }

        String suppliername = values.getAsString(BookEntry.COLUMN_BOOK_SUPPLIERNAME);
        if (suppliername == null){
            throw new IllegalArgumentException("Book requires a supplier");
        }

        String phonenumer = values.getAsString(BookEntry.COLUMN_BOOK_SUPPLIERPHONE);
        if (phonenumer == null){
            throw new IllegalArgumentException("Supplier requires a phonenumber");
        }

        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        long id = database.insert(BookEntry.TABLE_NAME, null, values);

        if (id == -1){
            Log.e(LOG_TAG, "Failed to insert row for " + uri);
            return null;
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return ContentUris.withAppendedId(uri, id);

    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        int rowsDeleted;

        final int match = sUriMatcher.match(uri);

        switch (match){
            case BOOKS:
                rowsDeleted = database.delete(BookEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case BOOK_ID:
                selection = BookEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsDeleted = database.delete(BookEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Deletion is not supported for " + uri);
        }

        if (rowsDeleted !=0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        final int match = sUriMatcher.match(uri);
        switch (match){
            case BOOKS:
                return updatePet(uri, contentValues, selection, selectionArgs);
            case BOOK_ID:
                selection = BookEntry._ID + "=?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                return updatePet(uri, contentValues, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Update is not supported for " + uri);
        }
    }

    public int updatePet(Uri uri, ContentValues values, String selection, String[] selectionArgs){
        if(values.containsKey((BookEntry.COLUMN_BOOK_NAME))){
            String name = values.getAsString(BookEntry.COLUMN_BOOK_NAME);
            if (name == null){
                throw new IllegalArgumentException("Book requires a name");
            }
        }

        if (values.containsKey(BookEntry.COLUMN_BOOK_PRICE)){
            String price = values.getAsString(BookEntry.COLUMN_BOOK_PRICE);
            if(price == null){
                throw new IllegalArgumentException("Book requires a price");
            }
        }

        if (values.containsKey(String.valueOf(BookEntry.COLUMN_BOOK_QUANTITY))){
            Integer quantity = values.getAsInteger(String.valueOf(BookEntry.COLUMN_BOOK_QUANTITY));

            if(quantity != null && quantity < 0){
                throw new IllegalArgumentException("Book requires valid quantity");
            }
        }

        if (values.containsKey(BookEntry.COLUMN_BOOK_SUPPLIERNAME)){
            String supplierName = values.getAsString(BookEntry.COLUMN_BOOK_SUPPLIERNAME);
            if (supplierName == null){
                throw new IllegalArgumentException("Book requires a supplier");
            }
        }

        if (values.containsKey(BookEntry.COLUMN_BOOK_SUPPLIERPHONE)){
            String supplierPhone = values.getAsString(BookEntry.COLUMN_BOOK_SUPPLIERPHONE);
            if (supplierPhone == null){
                throw new IllegalArgumentException("Book requires a phonenumber");
            }
        }

        if (values.size() == 0){
            return 0;
        }

        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        int rowsUpdated = database.update(BookEntry.TABLE_NAME, values, selection, selectionArgs);

        if (rowsUpdated !=0){
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsUpdated;
    }
}
